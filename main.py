import random
import string
import csv

MOBILE_OPERATOR_LIST = ('015', '016', '017', '018', '019')
GENDER_LIST = ('Male', 'Female', 'Other')

def get_name(name_length):
    """Returns a name of specified length using random number generator.

    :name_length: int
    :returns: str

    """

    name = ''.join(random.choice(string.ascii_uppercase) for _ in range(name_length))

    return name

def get_contact_number():
    """Returns a 11 digit number.

    :returns: str

    """
    opcode = random.choice(MOBILE_OPERATOR_LIST)
    digits = random.randint(1000000, 99999999)
    number = opcode + str(digits)

    return number

def get_age():
    """Returns a random number from 20 and 40

    :returns: int

    """
    return random.randint(20, 40)

def get_gender():
    """Returns random string from gender_list
    :returns: str

    """

    return random.choice(GENDER_LIST)

def create_csv():
    with open('contact_list.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['no.', 'first_name', 'last_name', 'gender', 'age', 'phone_number'])
        for i in range(100):
            first_name = get_name(8)
            last_name = get_name(7)
            gender = get_gender()
            age = get_age()
            phone_number = get_contact_number()

            writer.writerow([i+1, first_name, last_name, gender, age, phone_number])
    
    file.close()



def main():
    create_csv()


if __name__ == "__main__":
    main()
